package vichita.project.myfeedreader;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import vichita.project.myfeedreader.rss.RSSParser;
import vichita.project.myfeedreader.rss.domain.RSSItem;

public class NetworkActivity extends Activity {

    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
    public static String URL = "https://www.dropbox.com/s/ta0fvbibkjonzn6/test.xml";

    private static boolean wifiConnected = false;
    private static boolean mobileCOnnected = false;
    public static boolean refreshDisplay = true;
    public static String sPref = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.network_layout);

        Log.i("parser", "startactivity");
//        Bundle bundle = getIntent().getExtras();
//        URL = bundle.getString("url");
        //loadPage();
        new DownloadXmlTask().execute(URL);
    }

    public void loadPage() {
        if ((sPref.equals(ANY)) && (wifiConnected || mobileCOnnected)) {
            new DownloadXmlTask().execute(URL);
        } else if ((sPref.equals(WIFI)) && (wifiConnected)) {
            new DownloadXmlTask().execute(URL);
        } else {

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.network, menu);
        return true;
    }

    private class DownloadXmlTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... urls) {
            try {
                Log.i("parser", "start async");
                return loadXmlFromNetwork(urls[0]);
            } catch (IOException e) {
                return getResources().getString(R.string.connection_error);
            } catch (XmlPullParserException e) {
                return getResources().getString(R.string.xml_error);
            }
        }

        @Override
        protected void onPostExecute(String s) {
            setContentView(R.layout.network_layout);

//            WebView myWebView = (WebView) findViewById(R.id.webview);
//            myWebView.loadData(s, "text/html", null);

            TextView textView = (TextView) findViewById(R.id.textview);
            textView.setText(s);
        }
    }

    private String loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException{
        InputStream stream = null;

        RSSParser rssParser = new RSSParser();
        List<RSSItem> items = null;
        String title = null;
        String link = null;
        String description = null;
        Calendar rightNow = Calendar.getInstance();
        DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean pref = sharedPreferences.getBoolean("summaryPref", false);

        StringBuilder htmlString = new StringBuilder();
        //htmlString.append("<h3>" + "title" + "</h3>");
        //htmlString.append("<em>" + "updated" + " " + formatter.format(rightNow.getTime()) + "</em>");
        try {
            Log.i("parser", "get stream");
            stream = downloadUrl(urlString);

            /*//
            BufferedReader r = new BufferedReader(new InputStreamReader(stream));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }

            Log.i("parser", "stream: "+total);
            //
*/
            items = rssParser.parse(stream);
        }finally {
            if (stream != null) {
                stream.close();
            }
        }

       /* for (RSSItem item : items) {
            htmlString.append("<p><a href='");
            htmlString.append(item.getLink());
            htmlString.append("'>" + item.getTitle() + "</a></p>");

            if (pref) {
                htmlString.append(item.getDescription());
            }
        }*/
        if (items != null) {
            for (RSSItem item : items) {
                Log.i("parser", "title: " + item.getTitle());
                htmlString.append(item.getTitle() + "," + item.getLink());
            }
            Log.i("parser", "string" + htmlString.toString());
            return htmlString.toString();
        } else {
            return "no data";
        }


    }

    private InputStream downloadUrl(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(1000000);
        conn.setConnectTimeout(1500000);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);
        conn.connect();

        InputStream stream = conn.getInputStream();

        BufferedReader r = new BufferedReader(new InputStreamReader(stream));
        StringBuilder total = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            total.append(line);
        }

        Log.i("parser", "stream: "+total);

        return stream;
    }
    
}