package vichita.project.myfeedreader.rss.domain;


import java.util.List;

/**
 * Created by VIchita on 7/15/13 AD.
 */
public class RSSItem {
    private String title;
    private String link;
    private String description;
    private String comment;
    private List<Category> categories;
    private String creator;
    private Guid guid;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Guid getGuid(){
        return guid;
    }

    public void setGuid(boolean isPermaLink, String detail){
        this.guid = new Guid(isPermaLink,detail);
    }

    public void addCategory(String domain, String category) {
        categories.add(new Category(domain, category));
    }

    public List<Category> getCategories(){
        return categories;
    }

    protected class Guid{
        boolean isPermaLink;
        String detail;

        public Guid(boolean isPermaLink, String detail){
            this.isPermaLink = isPermaLink;
            this.detail = detail;
        }
    }

    protected class Category{
        String domain;
        String category;

        public Category(String domain, String category){
            this.domain = domain;
            this.category = category;
        }
    }
}
